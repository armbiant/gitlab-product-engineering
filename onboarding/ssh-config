##################################################
# GitLab Hosts
##################################################

## See also ssh-config-gke in this same directory
#
## Tip: you can also use this configuration by adding the following line into
## your ~/.ssh/config:
##
## Include ~/path/to/reliability/onboarding/ssh-config
## Include ~/path/to/reliability/onboarding/ssh-config-gke

## Set the username and PreferredAuthentications here to avoid duplication
Host *.gitlab-*.internal lb-bastion.*.gitlab.com
  ## If your remote username is different than your local one
  # User <unix username>

  PreferredAuthentications publickey

  ## Optional YubiKey cardno
  # IdentitiesOnly yes
  # IdentityFile cardno:000000000000
  ## ...or PGP key ID
  # IdentityFile openpgp:0xabcdef
  ## ...or PGP SSH public key export (gpg --export-ssh-key 0xabcdef > ~/.ssh/yubikey-gitlab.pub)
  # IdentityFile ~/.ssh/yubikey-gitlab.pub

  ## Alternative to setting $SSH_AUTH_SOCK
  # IdentityAgent ~/.gnupg/S.gpg-agent.ssh

  ## Multiplexing for speeeed
  ControlMaster auto
  ControlPath ~/.ssh/master-%C
  ControlPersist 30m
  ServerAliveInterval 30

## HAProxy nodes use a different port
Host fe-*.gitlab-*.internal haproxy*.gitlab-*.internal
  Port 2222

## Bastions for each GCP environment
Host lb-bastion.*.gitlab.com
  StrictHostKeyChecking no

Host *.c.gitlab-db-benchmarking.internal
  ProxyCommand ssh lb-bastion.db-benchmarking.gitlab.com -W %h:%p

Host *.gitlab-*.internal
  ProxyCommand ssh lb-bastion.$(echo "%h" | cut -d. -f1 | awk -F- '{print $NF}').gitlab.com -W %h:%p

## Console aliases
Host *-console
  CanonicalizeHostname yes

Host gprd-console
  HostName console-01-sv-gprd.c.gitlab-production.internal
  ProxyJump lb-bastion.gprd.gitlab.com
  ## Enable instead of ProxyJump above if you have gcloud installed and
  ## authenticated, and the necessary permissions in the gitlab-production
  ## project (roles/compute.instanceAdmin.v1 and/or roles/iap.tunnelResourceAccessor )
  # ProxyCommand gcloud compute start-iap-tunnel console-01-sv-gprd %p --listen-on-stdin --project=gitlab-production --zone=us-central1-b --verbosity=warning
  # ProxyUseFdpass no

Host gprd-console-ro
  HostName console-ro-01-sv-gprd.c.gitlab-production.internal
  ProxyJump lb-bastion.gprd.gitlab.com
  ## Enable instead of ProxyJump above if you have gcloud installed and
  ## authenticated, and the necessary permissions in the gitlab-production
  ## project (roles/compute.instanceAdmin.v1 and/or roles/iap.tunnelResourceAccessor )
  # ProxyCommand gcloud compute start-iap-tunnel console-ro-01-sv-gprd %p --listen-on-stdin --project=gitlab-production --zone=us-central1-b --verbosity=warning
  # ProxyUseFdpass no

Host gstg-console
  HostName console-01-sv-gstg.c.gitlab-staging-1.internal
  ProxyJump lb-bastion.gstg.gitlab.com
  ## Enable instead of ProxyJump above if you have gcloud installed and
  ## authenticated, and the necessary permissions in the gitlab-staging-1
  ## project (roles/compute.instanceAdmin.v1 and/or roles/iap.tunnelResourceAccessor )
  # ProxyCommand gcloud compute start-iap-tunnel console-01-sv-gstg %p --listen-on-stdin --project=gitlab-staging-1 --zone=us-central1-b --verbosity=warning
  # ProxyUseFdpass no

# vim:ft=sshconfig:
