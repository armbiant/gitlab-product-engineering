{ pkgs }:
let
in
# Calliope\ Build and patch drivestrike
pkgs.stdenv.mkDerivation rec {
  pname   = "drivestrike";
  version = "2.1.20-10";

  src = pkgs.fetchurl {
    url = https://app.drivestrike.com:443/static/apt/pool/main/d/drivestrike/drivestrike_2.1.20-10_amd64.deb;
    sha256 = "sha256-6GzmkeCKDj87psJr+LaXbdxY5GmsSc095O9gkIb4q2w=";
  };

  unpackPhase = ''
    dpkg-deb -x $src .
  '';

  buildPhase = ''
    cat << EOF > drivestrike-start
#!/usr/bin/env bash

if [ -f "/etc/drivestrike.conf" ]; then
  drivestrike run
else
  drivestrike register $GITLAB_EMAIL "" https://app.drivestrike.com:444/svc/
  drivestrike run
fi
EOF
  '';

  nativeBuildInputs = [
    pkgs.dpkg
    pkgs.dmidecode
    pkgs.autoPatchelfHook
    pkgs.libsoup
    pkgs.glib
  ];

  installPhase = ''
    mkdir -p $out/bin/
    chmod +x drivestrike-start
    cp drivestrike-start $out/bin/drivestrike-start
    cp -r usr/bin/drivestrike $out/bin/drivestrike
  '';
}