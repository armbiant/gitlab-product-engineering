<!--
This template is for GitLab Team Members seeking support from the [Reliability::Foundations team](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/foundations.html)
If you're not a GitLab team member, please see https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#external-customer-escalations for seeking support from us.

Please fill out the details below.
-->

## General Information:

- Point of contact for this request: [+ GitLab team member @user +]
- Related issue for context (if applicable): [+ link +]
- [Foundations owned service](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/foundations.html#services) this relates to: [+ service_label +]

## Details

[+ Please provide as much detail and context as possible]

## Priority

Please check one:

- [ ] Very urgent, blocking significant other work: ~"Priority::1"
- [ ] A blocker, but we have workarounds: ~"Priority::2"
- [ ] Not currently a blocker but will be soon: ~"Priority::3"
- [ ] Not likely to be a blocker, this is a nice-to-have improvement or suggestion: ~"Priority::4"
- [ ] Unsure

<!--
please do not edit the below
-->

/label ~"workflow-infra::Triage" ~"team::Foundations" ~"unblocks others"