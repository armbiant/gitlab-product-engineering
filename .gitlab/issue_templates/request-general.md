<!--
This template is for GitLab Team Members seeking support of SRE where there isn't an existing `request-*` template available.
If you're not a GitLab team member, please see https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#external-customer-escalations for seeking support from us.

Please fill out the details below.
-->

**Details**
 - Point of contact for this request: [+ @user +]
 - If a call is needed, what is the proposed date and time of the call:  [+ Date and Time +]
 - Additional call details (format, type of call): [+ additional details +]

**SRE Support Needed**
[+ Support Request Details +]

<!--
If you know the Service that you need support for, please assign the Service label
For example: ~"Service::Thanos" or ~"Service::CI Runners" or ~"Service::Redis", etc.

For a list of all available Services:
https://gitlab.com/gitlab-com/gl-infra/reliability/-/labels?page=2&search=Service%3A%3A&subscribed=
-->

<!--
please do not edit the below
-->

/label ~"workflow-infra::Triage" ~"team::Reliability" ~"unblocks others"
