<!-- title the issue: Incident Manager Offboarding - Team Member Name -->

This issue is to provide a checklist for removing someone from the incident Manager rotation.

## IM Offboarder Details

- Person being removed - {+ GitLab username +}
- Offboarder - @jarv
- Shift they are dropping from: (in UTC, example "04:00 - 8:00 UTC") - {+ 4 hour shift +}
- Date when they will drop - {example: 2022-03-17}
- Reason for Offboarding - {example: leaving company, changing roles, no longer eligable}

## IM Offboarding checklist:

- [ ] Put a reminder on your calendar to remove the person from the shift on or near the effective date.
- [ ] Remove the person from Pagerduty
- [ ] Announce the change in the `#imoc_general` channel and let the other shift members know that their slots may be changing
```
@channel Please be aware that we have updated the [IM PagerDuty schedule](https://gitlab.pagerduty.com/schedules#P2SF64D) MR_LINK. Because the change will not take effect immediately we are looking for someone to override the following shifts:

If you have any questions don't hesistate to reach out.
```

/label ~"Reliability::P2" ~"team::Reliability-Ops" ~"workflow-infra::Triage" ~"IM-Offboarding" ~"IM"
